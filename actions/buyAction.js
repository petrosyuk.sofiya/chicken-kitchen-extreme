const command = require("../resources/input_files/commandConfiguration.json");
const warehousesService = require("../servises/warehousesHandler");
const buyService = require("../servises/buyService");
const helpers = require("../helpers/helpers");
const kitchenHandler = require('../handlers/kitchenHandler');
const fileReader = require('../servises/fileReader');
const trashService = require("../servises/trashService");
const restaurantBudget = require("../servises/restaurantBudget")
const recommendationService = require("../servises/recommendationService");
const timeService = require('../servises/timeService');
const jsonData = require("../resources/input_files/data.json");
const food = jsonData.Food;
const base = jsonData["Base ingredients"];

const buyAction = (i, validBudget, filePathForOutput, dishes, trash, ) => {
    if (command[i[0].toLowerCase()] === 'yes') { //якщо команда дозволена
        trashService.checkIsPoisoned(trash, command["waste limit"]);
        let person = i[1];
        let malformedFood = false;
        let recommendItems = [];
        let order = 'Chicken';//default
        if (i[2] === 'Recommend') {
            recommendItems.push(i[3]);
        } else {
            order = i[2];
            malformedFood = kitchenHandler.isMalformedFood(order, dishes);
        }

        //Enzelt 6.11.2 Recommendation Service - get most expensive and correct(no allergies right budget) for client
        if (recommendItems.length > 0) {
            order = recommendationService.getRecommendDish(food, base, person, recommendItems, command["profit margin"], buyService).order;
            if (order === undefined || order === '') {
                const error = `For ${person} can not recommend anything`;
                const message = helpers.createAuditMessage(i, error);
                fileReader.appendFile (filePathForOutput, message);
                kitchenHandler.auditAction(message, i[0], i);
                return false;
            }
        }

        if (!malformedFood) { //якщо їжа є в меню
            if (validBudget && !trashService.getPoisoned()) {  //якщо ресторан не банкрут і не отруєний
                const localMax = kitchenHandler.findLocalMax(order, command);
                const warehouses = warehousesService.getWarehouses();
                const warehousesCopy = { ...warehouses };
                const warehouseCheckResult = warehousesService.checkDishIngredientsInWarehouse(order, warehousesCopy);
                restaurantBudget.array.push(0)
                if (warehouses[order] > 0 || !warehouseCheckResult) { // все є на складі
                    const res = buyService.buy(person, order, command["profit margin"], command["dishes with allergies"], command["total maximum"], localMax, trash);
                    const message = helpers.createAuditMessage(i, res.sendRes);
                    kitchenHandler.auditAction(message, i[0], i);

                    // Time
                    console.log('time for BUY', timeService.getTotalTimeForBuy(i));
                    timeService.setTimeToBuildFood();

                }
                else if(recommendItems.length > 0){ //if recommendation service return wrong one
                    this.buyAction(i, validBudget, filePathForOutput, dishes, trash);
                } else { ////якщо на складі не достатньо інградієнтів
                    const error = `ERROR. Lack of ingredients`;
                    const message = helpers.createAuditMessage(i, error);
                    fileReader.appendFile (filePathForOutput, message);
                    kitchenHandler.auditAction(message, i[0], i);
                }
            } else
            if (trashService.getPoisoned()) { // ресторан отруєно
                kitchenHandler.messagePoisoned(filePathForOutput)
            } else { //ресторан банкрут
                kitchenHandler.sendRestaurantBudget();
            }
        } else { // якщо їжа не в меню
            const error = `${person} can’t buy ${order}, cook has no idea how to make it`;
            const message = helpers.createAuditMessage(i, error);
            fileReader.appendFile (filePathForOutput, message);
            kitchenHandler.auditAction(message, i[0], i);
        }
    } else {
        helpers.disabler(i)
    }
};

module.exports = { buyAction };