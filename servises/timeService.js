class TimeService {
    constructor() {
        this.timeToBuildFood = [];
        this.totalTime = 0;
    }

    addTimeToTotal(time) {
        this.totalTime += time;
    }

    getTotalTime() {
        return this.totalTime;
    }

    checkEvery100units() {
        const totalTime = this.getTotalTime();
        if (Math.floor(totalTime / 100) > 0) {
            this.totalTime -= Math.floor(totalTime / 100);
            return Math.floor(totalTime / 100);
        } return 0;
    }

    setTimeToBuildFood() {
        this.timeToBuildFood = [];
    }

    getNumberOfVowels(commandInput) {
        const string = commandInput.toString()
        const numberOfVowel = string.match(/[aeiouy]/gi);
        return numberOfVowel === null ? 0 : numberOfVowel.length; //12
    }

    getRandomForBaseTime(str) {
        const randomlyPercent = 20; //20%
        const baseTime = this.getNumberOfVowels(str);
        const getPercentOfRandomly = randomlyPercent * baseTime / 100;
        const max = baseTime + getPercentOfRandomly + 1;
        const min = baseTime - getPercentOfRandomly;

        const randomForBaseTime = Math.random() * (max - min) + min;
        return Math.floor(randomForBaseTime); //  [9,15]
    }

    pushTimeToArray(str) {
        const ingredientTime = this.getNumberOfVowels(str)
        this.timeToBuildFood.push(ingredientTime);
    }

    calculateTimeFromArray() {
        return this.timeToBuildFood.reduce((previousValue, currentValue) => {
            return previousValue + currentValue
        }, 0);
    }

    totalBaseTimeForBuy(commandString) {
        const timeFromCommandString = this.getRandomForBaseTime(commandString);
        const timeToBuildFood = this.calculateTimeFromArray();
        return timeFromCommandString + timeToBuildFood;
    }

    getTime(action, commandString, amount, coefficient, customers) {
        let time;
        switch (action) {
            case 'Buy':
                time = this.getTotalTimeForBuy(commandString);
                break;
            case 'Table':
                time = this.getTotalTimeForTable(commandString, customers);
                break;
            case 'Order':
                time = this.getTotalTimeForOrder(amount, coefficient);
                break;
            case 'Budget':
                time = this.getTotalTimeForBudget();
                break;
            case 'Audit':
                time = this.getTotalTimeForAudit();
                break;
            default:
                time = this.getTotalTimeForOther();
                console.log('default time switcher');
        }
        return time;
    }

    getTotalTimeForBuy(commandString) {
        const totalBaseTimeForBuy = this.totalBaseTimeForBuy(commandString);
        return totalBaseTimeForBuy + 5;
    }
    getTotalTimeForOrder(amount, coefficient) {
        const sum =  (10 * amount) * coefficient;
        return Math.floor(sum + 1000);
    }
    getTotalTimeForTable(commandString, customers) {
        const totalBaseTimeForBuy = this.totalBaseTimeForBuy(commandString);
        return Math.floor(totalBaseTimeForBuy * (1 + (0.05 ** customers.length)));
    }
    getTotalTimeForBudget() {
        return 250;
    }
    getTotalTimeForAudit() {
        return 100;
    }
    getTotalTimeForOther() {
        return 1;
    }
}

const timeService = new TimeService();

module.exports = timeService;