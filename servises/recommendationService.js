const {checkAllIngredients} = require("../helpers/helpers");
const budgetData = require("../resources/input_files/budget.json");
const budget = budgetData['Regular customer budget'];
const statisticService = require("../servises/statisticService");

class RecommendationService {
	getRecommendDish(food, base, person, recommendItems, margin, buyService) {
		let recommendDishMain = '';

		let recommendDishes = [];

		for (const [foodItem, foodItems] of Object.entries(food)) {
			const userIngredients = [];
			checkAllIngredients(foodItem, userIngredients, food, base);
			let countOfCoincidence = 0;
			for (const recommendItem of recommendItems) {
				if(userIngredients.includes(recommendItem)) {
					countOfCoincidence += 1;
				}
			}
			if(countOfCoincidence > 0) {
				recommendDishes.push(
					{
						order: foodItem,
						countOfCoincidence: countOfCoincidence,
						userIngredients: userIngredients,
						sum: buyService.getSum(userIngredients, margin),
					}
				);
			}
		}

		recommendDishes.sort((a, b) => (a.sum < b.sum) ? 1 : -1);

		for (const recommendDish of recommendDishes) {
			const foundAllergy = buyService.getAllergies(person, recommendDish.userIngredients)[0] || '';
			if (budget[person] > recommendDish.sum && foundAllergy === '') {
				recommendDishMain = recommendDish; break;
			}

		}
		statisticService.addToDailyPopularRecommendations(recommendDishMain.order);
		return recommendDishMain;
	}
}

const recommendationService =  new RecommendationService();
module.exports = recommendationService;