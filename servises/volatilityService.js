const warehouseService = require("../servises/warehousesHandler");
const volatilityConfig = require("../resources/input_files/commandConfiguration.json");

class VolatilityService {
    getVolatilityNum(volatilityConfig) {
        return volatilityConfig / 100;
    }

    getRandomVolatility(volatilityConfig) {
        const volatilityNum = this.getVolatilityNum(volatilityConfig);
        const max = 1 + volatilityNum; //1.1
        const min = 1 - volatilityNum; //0.9

        const volatility = Math.random() * (max - min) + min;
        return parseFloat(volatility.toFixed(2))
    }

    volatility(order) {
        let volatility;

        if (warehouseService.checkIsDish(order).length === 0) {
            volatility = this.getRandomVolatility(volatilityConfig["order ingredient volatility"])
        } else {
            volatility = this.getRandomVolatility(volatilityConfig["order dish volatility"])
        }
        console.log(`volatility for ${order} = `,volatility);
        return volatility;
    }
}

const volatilityService = new VolatilityService();

module.exports = volatilityService;