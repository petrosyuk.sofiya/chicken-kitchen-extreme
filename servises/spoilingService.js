class SpoilingService {
    randomizer = (spoilRate) => { // повертає число від 1 до (100 / на те що в конфіг)
        if(spoilRate !== 0) {
            return Math.floor(Math.random() * (100/spoilRate))
        } else return 0
    }

    checkAmountOfSpoiling = (ingredientQuantity, spoilRate) => {
        let amountOfSpoiling = 0;
        for(let i=0; i<ingredientQuantity; i++) {
            const random = this.randomizer(spoilRate);
            if (random === 1) {
                amountOfSpoiling += 1;
                i ++;
            }
        }
        return amountOfSpoiling // к-сть зіпсованих продуктів
    }

    checkSpoiled(ingredient, number, RandomService, config) {
        let spoiledAmount = 0;
        for (let i = 0; i < number; i++) {
            if (RandomService.getRandom() === config) {
                spoiledAmount += 1;
            }
        }
        return spoiledAmount;
    }

    // randomGeneratorForBuy(spoilRate) {
    //     if(spoilRate !== 0) {
    //         return Math.floor(Math.random() * (100/spoilRate))
    //     } else return 0
    // }

    checkSpoiledForBuy(ingredients, number, RandomService, config) {
        let spoiled = [];
        for(const ingredient of ingredients) {
            if (this.randomizer(config) === 1) {

                let found = false;
                spoiled.map((item) => {
                    if (item.ingredient === ingredient) {
                        item.qty += 1;//number if needed
                        found = true;
                    }
                });
                if (!found) {
                    spoiled.push({
                        ingredient: ingredient,
                        qty: 1,//number if needed
                    });
                }
            }
        }
        return spoiled;
    }
}

const spoilingService = new SpoilingService();

module.exports = spoilingService;