class WantsService {
	getWants(customerBudget, RandomService, base, ingredients, tipsAmount, total) {
		let wantsCount = 0;

		const random = RandomService.getRandomWants()

		if (random > 0 && random <= 0.5) {
			wantsCount = 0
		} else if (random > 0.5 && random <= 0.85 ) {
			wantsCount = 1
		} else if (random > 0.85 && random <= 0.95 ) {
			wantsCount = 2
		} else if (random > 0.95 && random <= 1) {
			wantsCount = 3
		}

		if (wantsCount > 0) {
			let concreteIngredientsForCheck = [];
			concreteIngredientsForCheck = this.getConcreteIngredientsForCheck(RandomService, ingredients, wantsCount, concreteIngredientsForCheck)
			const equalWants = this.checkEqualWants(ingredients, concreteIngredientsForCheck);
			if (equalWants.length === 0) {
				return false;
			}

			if (tipsAmount <= 0) {
				tipsAmount = total * 0.1;// need to get config percent for tips not hardcode
			}
			tipsAmount = parseFloat((tipsAmount * (2**equalWants.length)).toFixed(2));
			if (tipsAmount > customerBudget) {
				tipsAmount = customerBudget;
			}
			return {
				tipsAmount: tipsAmount,
				ingredients: equalWants
			};
		} else {
			return false;
		}

	}

	getConcreteIngredientsForCheck(RandomService, ingredients, wantsCount, concreteIngredients) {
		for(const ingredient of ingredients) {
			if (concreteIngredients.length === wantsCount) {
				return concreteIngredients;
			}
			if (RandomService.getRandom() > 0.5) {
				concreteIngredients.push(ingredient);
			}
		}
		if (concreteIngredients.length < wantsCount) {
			this.getConcreteIngredientsForCheck(RandomService, ingredients, wantsCount, concreteIngredients);
		}
	}

	checkEqualWants(ingredients, concreteIngredientsForCheck) {
		let equalWants = [];
		if (!Array.isArray(concreteIngredientsForCheck)) {
			return equalWants;
		}
		for (const concreteIngredientForCheck of concreteIngredientsForCheck) {
			if (ingredients.includes(concreteIngredientForCheck)) {
				equalWants.push(concreteIngredientForCheck);
			}
		}
		return equalWants;
	}

}


const wantsService = new WantsService();
module.exports = wantsService;