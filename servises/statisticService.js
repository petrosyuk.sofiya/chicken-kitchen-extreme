const jsonData = require("../resources/input_files/data.json");
const base = jsonData['Base ingredients'];

class StatisticService {
	constructor() {
		//Enzelt - 6.12.1 - add statistic
		this.dailyPopularIngredients = [];
		this.dailyDishesTotal = 0;
		this.dailyPopularRecommendations = [];
		this.dailyProfitDishes = [];
		this.baseIngredientsStatistic = {};
		this.commandIterator = {};
	}


	addToDailyDishesTotal(qty) {
		this.dailyDishesTotal += qty;
	}

	getDailyDishesTotal() {
		return this.dailyDishesTotal;
	}

	addToDailyPopularIngredients(ingredients = []) {
		for(const ingredient of ingredients) {
			let exist = false;
			for (const popularIngredient of this.dailyPopularIngredients) {
				if (popularIngredient.ingredient === ingredient) {
					popularIngredient.total += 1;// can add qty id needed
					exist = true;
				}
			}
			if (!exist) {
				this.dailyPopularIngredients.push(
					{
						ingredient: ingredient,
						total: 1,
					});
			}
		}
	}

	getDailyPopularIngredients() {
		return this.dailyPopularIngredients;
	}

	getDailyMostPopularIngredient() {
		if (this.dailyPopularIngredients.length > 1) {
			this.dailyPopularIngredients =
				this.dailyPopularIngredients.sort((a, b) => (a.total < b.total) ? 1 : -1)
			return this.dailyPopularIngredients[0].ingredient;
		} else return '';
	}

	addToDailyPopularRecommendations(ingredient) {
		let exist = false;
		for (const popularIngredient of this.dailyPopularRecommendations) {
			if (popularIngredient.ingredient === ingredient) {
				popularIngredient.total += 1;// can add qty id needed
				exist = true;
			}
		}
		if (!exist) {
			this.dailyPopularRecommendations.push(
				{
					ingredient: ingredient,
					total: 1,
				});
		}

	}

	getDailyPopularRecommendation() {
		return this.dailyPopularRecommendations;
	}

	getDailyMostPopularRecommendation() {
		if (this.dailyPopularRecommendations.length > 1) {
			this.dailyPopularRecommendations =
				this.dailyPopularRecommendations.sort((a, b) => (a.total < b.total) ? 1 : -1)
			return this.dailyPopularRecommendations[0].ingredient;
		} else return ''
	}

	addToDailyProfitDishes(ingredient, profit) {
		let exist = false;
		for (const popularIngredient of this.dailyProfitDishes) {
			if (popularIngredient.ingredient === ingredient) {
				popularIngredient.total += 1;// can add qty id needed
				popularIngredient.profit += profit;
				exist = true;
			}
		}
		if (!exist) {
			this.dailyProfitDishes.push(
				{
					ingredient: ingredient,
					total: 1,
					profit: profit,
				});
		}

	}

	getDailyProfitDishes() {
		return this.dailyProfitDishes;
	}

	getDailyMostProfitDish() {
		if (!this.dailyProfitDishes.length) {
			return '';
		} else {
			this.dailyProfitDishes =
				this.dailyProfitDishes.sort((a, b) => (a.profit < b.profit) ? 1 : -1);
			return this.dailyProfitDishes[0];
		}
	}

	addToBaseIngredientStatistic(warehouse, command) {
		if (this.commandIterator[command] === undefined) {
			this.commandIterator[command] = {number: 1}
		} else {
			this.commandIterator[command] = {number: this.commandIterator[command].number + 1}
		}
		this.baseIngredientsStatistic[command + '-' + this.commandIterator[command].number] = [];
		for(const [ingredient, qty] of Object.entries(warehouse)) {
			if (!base.includes(ingredient)) continue;
			this.baseIngredientsStatistic[command + '-' + this.commandIterator[command].number].push(
				{
					ingredient: ingredient,
					qty: qty,
				}
			);
		}
	}

	getBaseIngredientsStatistic() {
		const length = Object.keys(this.baseIngredientsStatistic).length;
		let ingredientForSkip = [];
		let first = [];
		let last = [];
		let i = 0;
		for(const [command, ingredients] of Object.entries(this.baseIngredientsStatistic)) {
			if (i === 0) {
				first = ingredients;
			}
			if (i === length - 1) {
				last = ingredients;
			}
			i++;
		}
		let differencePercent = 0;
		for (let i = 0; i < first.length; i++) {
			differencePercent = (last[i].qty * 100) / first[i].qty;
			if (100 >= differencePercent) {
				differencePercent = 100 - differencePercent;
			} else {
				differencePercent = differencePercent - 100;
			}
			if (differencePercent < 20) {
				ingredientForSkip.push(last[i].ingredient);
			}
		}

		let newBaseIngredientsStatistic = {};
		for(let [command, ingredients] of Object.entries(this.baseIngredientsStatistic)) {
			let ingredientsNew = [];
			for(let i = 0; i < ingredients.length; i++) {
				if (!ingredientForSkip.includes(ingredients[i].ingredient)) {
					ingredientsNew.push(ingredients[i]);
				}
			}
			if (newBaseIngredientsStatistic[command] === undefined) {
				newBaseIngredientsStatistic[command] = ingredientsNew;
			}
		}

		return newBaseIngredientsStatistic;
	}

}

const statisticService = new StatisticService();

module.exports = statisticService;