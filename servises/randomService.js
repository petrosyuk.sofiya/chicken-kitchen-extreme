class RandomService {
	getRandom() {
		return parseFloat(Math.random().toFixed(1));
	}
	getRandomWants() {
		return parseFloat(Math.random().toFixed(2));
	}
}

const randomService = new RandomService();
module.exports = randomService;