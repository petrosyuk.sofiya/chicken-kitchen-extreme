class TipsService {
	constructor() {
		this.tips = 0;
	}

	addTips(tips) {
		this.tips += tips;
	}

	getTips() {
		return this.tips;
	}

	checkTips(customerBudget, sum, RandomService, config) {
		let tipsAmount = 0;
		if (RandomService.getRandom() < 0.5) {
			return tipsAmount;
		}
		tipsAmount = sum * config / 100;

		if (tipsAmount > customerBudget) {
			tipsAmount = customerBudget;
		}

		this.addTips(tipsAmount);

		return tipsAmount;
	}
}

const tipsService = new TipsService();
module.exports = tipsService;