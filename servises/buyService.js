const jsonData = require('../resources/input_files/data.json');
const priceData = require('../resources/input_files/price.json');
const budgetData = require('../resources/input_files/budget.json');
const messageCodes = require('../resources/messageCodes.json');
const commandConfiguration = require('../resources/input_files/commandConfiguration.json');
const fileReader = require('./fileReader');
const restaurantBudgetService = require('./restaurantBudget');
const warehousesService = require('./warehousesHandler');
const taxService = require('./taxService');
const discountService = require('./discountService');
const orderService = require("./orderService");
const tipsService = require("./tipsService");
const RandomService = require("./randomService");
const {checkAllIngredients} = require("../helpers/helpers");
const trashService = require("../servises/trashService");
const audit = require("../servises/audit")
const spoilingService = require("../servises/spoilingService");
const wantsService = require("../servises/wantsService");
const recommendationService = require("../servises/recommendationService");
const command = require("../resources/input_files/commandConfiguration.json");
const statisticService = require("../servises/statisticService");
const timeService = require("../servises/timeService");

const filePathForOutput = './resources/output_files/output.txt';
const regularCustomer = jsonData['Regular customer'];
const food = jsonData.Food;
const base = jsonData['Base ingredients'];
const price = priceData['Base ingredients'];
const budget = budgetData['Regular customer budget'];


class BuyService {
    constructor() {
        this.clientBudget = {};
        this.price = 0
        this.name = ""
    }

    getAllergies = (name, userIngredients) => {
        const allergies = regularCustomer[name];
        const foundAllergy = allergies.find(element => {
            return userIngredients.find(item => {
                if (item === element) {
                    return true
                }
            });
        });
        return foundAllergy ? [foundAllergy] : []; //ф-я поверне алергію, якщо знайде, якщо ні - порожній масив
    };

    profitMargin = margin => {
        const getMargin = this.getMargin(margin)
        return 1 + getMargin / 100
    }

    getMargin = margin => {
        return margin ? margin : 30
    }

    getSum = (ingredients, margin) => {
        const sumArray = [];
        const profitMargin = this.profitMargin(margin)
        ingredients.forEach(i => sumArray.push(price[i]));

        this.price = Math.ceil(sumArray.reduce((total, amount) => total + amount) * profitMargin)
        return Math.ceil(sumArray.reduce((total, amount) => total + amount) * profitMargin);
    };

    discount = (name, sum, discount) => {
        const sumOfDiscount = discountService.makeDiscount(name, sum, discount)
        return sumOfDiscount > 0 ? sumOfDiscount : 0
    }

    getTotalBudget = (name, sum, totalBudget, discountValue) => {

        const discount = this.discount(name, sum, discountValue)
        console.log('totalBudget', name, totalBudget - sum + discount);
        console.log('discount', name, discount);

        this.name = name

        return totalBudget - sum + discount;
    };


    //функція перевіряє на алегрію, брак грошей в клієнта, або саксес
    sendResult = (foundAllergies, name, order, sum, configuration, totalMax, localMax, ingredients, trash, foundSpoiled, base) => {
        const warehouses = warehousesService.getWarehouses();
        if (foundAllergies) {
            this.dishesWithAllergies(configuration, order, warehouses, totalMax, localMax, ingredients, commandConfiguration["transaction tax"], trash);
            return `${name} can’t order ${order}, allergic to: ${foundAllergies}`
        } else if (this.clientBudget[name] < sum) {
            return `${name} – can’t order, budget ${this.clientBudget[name]} and ${order} costs ${sum}`
        }
        else {
            let messageAboutSpoil = '';
            if (foundSpoiled.length > 0) {
                for(const spoiledItem of foundSpoiled) {
                    trashService.trashService(command["wasteLimit"], trash, spoiledItem.qty, spoiledItem.ingredient);
                    warehousesService.reduceQuantities(spoiledItem.ingredient, warehouses);
                    messageAboutSpoil += `${spoiledItem.ingredient} - spoiled ${spoiledItem.qty}; `;
                        }
            } else messageAboutSpoil = 0;
            discountService.addPerson(name);
            this.clientBudget[name] = this.getTotalBudget(name, sum, this.clientBudget[name], commandConfiguration["every third discount"]);

            //Enzelt 6.9.1, 6.10.1 need to add tips logic here and than calculate tips tax
            let tipsAmount = tipsService.checkTips(
                this.clientBudget[name],
                sum,
                RandomService,
                commandConfiguration['max tip']
            );

            //Enzelt 6.11.1 wants
            const wants = wantsService.getWants(this.clientBudget[name], RandomService, base, ingredients, tipsAmount, sum);
            if (wants) {
                tipsAmount = wants.tipsAmount;
            }
            let wantsMessage = '';
            (wants.ingredients !== undefined)
                ? wantsMessage = wants.ingredients.join(', ')
                : wantsMessage = 'no wants';
            this.clientBudget[name] = this.getTotalBudget(name, tipsAmount, this.clientBudget[name], commandConfiguration["every third discount"]);

            restaurantBudgetService.increaseRestaurantBudget(name, sum, commandConfiguration["transaction tax"], commandConfiguration["every third discount"], tipsAmount);

            taxService.addAlreadyCollectedTax(sum, commandConfiguration["transaction tax"]);
            taxService.addToDailyTipsTax(tipsAmount, commandConfiguration["tips tax"]);

            warehousesService.reduceQuantities(order, warehouses);

            const sumOfDiscount = discountService.makeDiscount(name, sum, commandConfiguration["every third discount"]);
            const tax = taxService.transactionTaxSum(sum, commandConfiguration["transaction tax"]);
            const discount = sumOfDiscount > 0 ? `, discount = ${sumOfDiscount}` : '';
            return `${name} - ${order} costs ${sum}: success, tax = ${tax}${discount}, tips = ${tipsAmount}, wants = ${wantsMessage}, spoiled: ${messageAboutSpoil}`;
        }
    };

    buy = (person, order, margin, configuration, totalMax, localMax, trash, checkSpoil = true, foundSpoiled = []) => {
        const userIngredients = [];
        checkAllIngredients(order, userIngredients, food, base);

        const foundAllergy = this.getAllergies(person, userIngredients)[0] || '';
        //потрібно додатково прибрати зіпсовані зі складу і зробити перевірку на наявність зіпсованих ще раз,
        // щоб все ж таки успішно приготувати страву або не пригоиуваим
        //6.8.2 Enzelt check spoiled

        if(checkSpoil) {
            foundSpoiled = spoilingService.checkSpoiledForBuy(
                userIngredients,
                1,
                RandomService,
                commandConfiguration["spoil rate"]
            );

            if(foundSpoiled.length > 0) {
                return this.buy(person, order, margin, configuration, totalMax, localMax, trash, false, foundSpoiled);
            }
        }

        if (userIngredients.length === 0) {
            const a = 0;
        }
        let sum = this.getSum(userIngredients, margin);
        if (!this.clientBudget[person] && this.clientBudget[person] !== 0) {
            this.clientBudget[person] = budget[person];
        }

        //Enzelt 6.12.1 - add to statistic
        statisticService.addToDailyPopularIngredients(userIngredients);
        statisticService.addToDailyDishesTotal(1);
        statisticService.addToDailyProfitDishes(order, sum);

        const sendRes = this.sendResult(foundAllergy, person, order, sum, configuration, totalMax, localMax, userIngredients, trash, foundSpoiled, base);
        fileReader.appendFile(filePathForOutput, sendRes);
        return {sendRes, sum};
    }

    buyForTable = (person, order, margin, pooled) => {
        const userIngredients = [];
        checkAllIngredients(order, userIngredients, food, base);

        const foundAllergy = this.getAllergies(person, userIngredients)[0] || '';

        //time
        // timeService.setTimeToBuildFood();
        userIngredients.forEach(ingredient => timeService.pushTimeToArray(ingredient));


        //6.8.2 Enzelt check spoiled
        // const foundSpoiled = spoilingService.checkSpoiledForBuy(
        //     food[order],
        //     1,
        //     RandomService,
        //     commandConfiguration["spoil rate"]
        // );
        let foundSpoiled = [];
        let sum = this.getSum(userIngredients, margin);
        if (!this.clientBudget[person] && this.clientBudget[person] !== 0) {
            this.clientBudget[person] = budget[person];
        }
        if (foundAllergy) {
            return {
                code: messageCodes.allergy,
                message: `FAILURE. ${person} can’t order ${order}, allergic to: ${foundAllergy}. So, whole table fails.`
            };
        } else if (foundSpoiled.length !== 0) {
            let message = '';
            for(const spoiledItem of foundSpoiled) {
                message += `${spoiledItem.ingredient} - spoiled ${spoiledItem.qty}; `;
            }
            return {
                code: messageCodes.error,
                message: `FAILURE. ${person} can’t order ${order}, some spoiled ingredients: ${message}. So, whole table fails.`
            }
        } else if (this.clientBudget[person] < sum) {
            let surcharge = 0;
            let canPay = 0;
            if (pooled) {
              surcharge = sum - this.clientBudget[person];
              canPay = sum - surcharge;

              const tax = taxService.transactionTaxSum(canPay, commandConfiguration["transaction tax"]);

              statisticService.addToDailyPopularIngredients(userIngredients);
              statisticService.addToDailyDishesTotal(1);
              statisticService.addToDailyProfitDishes(order, sum);

              return {
                  code: messageCodes.success,
                  sum,
                  tax,
                  surcharge: surcharge,
                  message: `${person} - ${order} costs ${sum}: success, tax = ${tax}, pay: ${canPay}, surcharge: ${surcharge}`
              };

            } else {
                return {
                    code: messageCodes.budget,
                    message: `FAILURE. ${person} – can’t order, budget ${this.clientBudget[person]} and ${order} costs ${sum}. So, whole table fails.`
                };
            }
        } else {
            const tax = taxService.transactionTaxSum(sum, commandConfiguration["transaction tax"]);

            statisticService.addToDailyPopularIngredients(userIngredients);
            statisticService.addToDailyDishesTotal(1);
            statisticService.addToDailyProfitDishes(order, sum);

            return {
                code: messageCodes.success,
                sum,
                tax,
                message: `${person} - ${order} costs ${sum}: success, tax = ${tax}`
            };
        }
    }

    table = (customers, dishes, margin, pooled) => {
        const resArr = [];
        let surcharge = 0;
        //chek budget, allergies, surcharge here
        customers.forEach((customer, index) => {
            if (dishes[index].search('Recommend') !== -1) {
                dishes[index] = recommendationService.getRecommendDish(food, base, customer, dishes[index].split(', '), command["profit margin"], this).order;
            }

            const res = this.buyForTable(customer, dishes[index], margin, pooled);
            if (res.surcharge !== undefined) {
                surcharge += res.surcharge;
            }
            resArr.push(res);
        });

        const checkResSuccess = resArr.every(res => res.code === messageCodes.success);

        if (checkResSuccess) {
            const totalSum = resArr.reduce((previousValue, currentValue) => {
                return previousValue + currentValue.sum;
            }, 0);
            const totalTax = resArr.reduce((previousValue, currentValue) => {
                return previousValue + currentValue.tax;
            }, 0)
            let totalTips = 0;
            let wants = false;
            fileReader.appendFile(filePathForOutput, `Success: money amount ${totalSum}; tax amount ${totalTax}\n{`);
            customers.forEach((customer, index) => {
                const warehouses = warehousesService.getWarehouses();
                discountService.addPerson(customer);
                const sumOfDiscount = discountService.makeDiscount(customer, resArr[index].sum, commandConfiguration["every third discount"]);
                const discount = sumOfDiscount > 0 ? `, discount = ${sumOfDiscount}` : '';
                this.clientBudget[customer] = this.getTotalBudget(customer, resArr[index].sum, this.clientBudget[customer], commandConfiguration["every third discount"]);


                //Enzelt 6.9.1, 6.10.1
                let tipsAmount = tipsService.checkTips(
                    this.clientBudget[customer],
                    resArr[index].sum,
                    RandomService,
                    commandConfiguration['max tip']
                );
                totalTips += tipsAmount;

                const userIngredients = [];
                checkAllIngredients(dishes[index], userIngredients, food, base);
                wants = wantsService.getWants(this.clientBudget[customer], RandomService, base, userIngredients, tipsAmount, resArr[index].sum);
                if (wants) {
                    tipsAmount = wants.tipsAmount;
                }

                this.clientBudget[customer] = this.getTotalBudget(customer, tipsAmount, this.clientBudget[customer], commandConfiguration["every third discount"]);

                //Enzelt 6.10.3 - surcharge
                //maybe we need to add message about surcharging
                if (surcharge > 0 && this.clientBudget[customer] >= surcharge) {
                    this.clientBudget[customer] = this.getTotalBudget(customer, surcharge, this.clientBudget[customer], commandConfiguration["every third discount"]);
                    surcharge = 0;
                } else if (surcharge > 0 && this.clientBudget[customer] > 0 && this.clientBudget[customer] < surcharge){
                    const canPay = surcharge - this.clientBudget[customer];
                    this.clientBudget[customer] = this.getTotalBudget(customer, canPay, this.clientBudget[customer], commandConfiguration["every third discount"]);
                    surcharge = surcharge - canPay;
                }


                restaurantBudgetService.increaseRestaurantBudget(customer, resArr[index].sum, commandConfiguration["transaction tax"], commandConfiguration["every third discount"], tipsAmount);

                taxService.addAlreadyCollectedTax(resArr[index].sum, commandConfiguration["transaction tax"]);
                taxService.addToDailyTipsTax(tipsAmount, commandConfiguration['tips tax']);

                warehousesService.reduceQuantities(dishes[index], warehouses);
                fileReader.appendFile(filePathForOutput, `    ${resArr[index].message}${discount}`);
            });
            fileReader.appendFile(filePathForOutput, `}`);
            if (surcharge > 0) {
                return {message: messageCodes.error + '; table - low budget, no one can pay surcharge', totalTax, totalSum, totalTips}
            }
            return {message: messageCodes.success, totalTax, totalSum, totalTips, wants}
        } else {
            const errorMessage = resArr.find(res => {
                return res.code !== messageCodes.success;
            }).message;
            fileReader.appendFile(filePathForOutput, errorMessage);

            return {message: errorMessage};
        }
    }

    keep = (order, warehouses, totalMax, localMax, ingredients, transactionTax, sum) => {
        const totalSum = warehousesService.getTotalSumFromWarehouse(warehouses);
        const localSum = warehousesService.getAmountFromWarehouse(warehouses, order)
        if (1 + totalSum <= totalMax && 1 + localSum <= localMax) {
            restaurantBudgetService.restaurantBudget -= sum.orderSum + sum.extraSum;
            if (warehousesService.checkIsDish(order).length > 0 && warehouses[order] === 0) {
                warehousesService.reduceQuantities(order, warehouses);
                warehousesService.addIngredients(warehouses, order, 1);
            }
        }
        console.log('keep');
    }

    dishesWithAllergies = (configuration, order, warehouses, totalMax, localMax, ingredients, transactionTax, trash) => {
        const sum = orderService.sumForKeepedOrder(ingredients, 0, transactionTax);

        switch (configuration) {
            case 'waste':
                warehousesService.reduceQuantities(order, warehouses);
                ingredients.forEach(everyIngredient => {
                    trashService.trashService(commandConfiguration["waste limit"], trash, 1, everyIngredient)
                    const ingredientPrice = price[everyIngredient];
                    taxService.addToDailyTrashTax(ingredientPrice, commandConfiguration["waste tax"]);
                });
                break;
            case 'keep':
                this.keep(order, warehouses, totalMax, localMax, ingredients, transactionTax, sum);
                break;
            default:
                if (sum.orderSum <= configuration) {
                    warehousesService.reduceQuantities(order, warehouses);
                    ingredients.forEach(everyIngredient => {
                        trashService.trashService(commandConfiguration["waste limit"], trash, 1, everyIngredient);
                        const ingredientPrice = price[everyIngredient];
                        taxService.addToDailyTrashTax(ingredientPrice, commandConfiguration["waste tax"]);
                    });
                } else {
                    console.log(configuration)
                    this.keep(order, warehouses, totalMax, localMax, ingredients, transactionTax, sum);
                }
        }
    }

    //Old one version of getting tips and randomizer, leave here
    // getTips = (value) => {
    //
    //     // if
    //     var result1 = commandConfiguration["max tip"]
    //     var result2 = this.getRandomInt(1, 3)
    //     if (result2 === 2) {
    //         var result3 = this.getRandomInt(1, result1)
    //         var result5 = this.price / 100 * result3
    //         // console.log(result3 + " %")
    //         // console.log(parseFloat(result5.toFixed(1)))
    //         if ((this.clientBudget[this.name] - result5) < 0){
    //             return this.clientBudget[this.name]
    //         }else{
    //             return parseFloat(result5.toFixed(1))
    //         }
    //     } else {
    //         return 0
    //     }
    // }
    //
    // getRandomInt = (min, max) => {
    //     min = Math.ceil(min);
    //     max = Math.floor(max);
    //     return Math.floor(Math.random() * (max - min)) + min;
    // }
    //**************
}

const buyService = new BuyService();

module.exports = buyService;