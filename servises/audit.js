const fileReader = require('./fileReader');
const taxService = require("../servises/taxService");
// const buyService = require("../servises/buyService")
// const TipsService = require("../servises/tipsService");
// const restaurantBudget = require("./restaurantBudget")
const statisticService = require('../servises/statisticService');

const filePathForAudit = './resources/output_files/audit.txt';

class Audit {
    constructor() {
        this.auditData = [];
    }

    sortDataByTime() {
        Array.prototype.sortBy = function(p) {
            return this.slice(0).sort(function(a,b) {
                return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
            });
        }

        return this.auditData.sortBy('time');
    }

    init = () => {
        const message =
            `INIT
            Warehouses: ${JSON.stringify(this.auditData[0].initialWarehouses)}
            Restaurant Budget: ${this.auditData[0].initialBudget}
            Daily Tax: ${this.auditData[0].initialDailyTax}\r\n
            START
            `
        ;
        fileReader.appendFile(filePathForAudit, message)
    }

    end = (tax, endRestaurantBudget, startRestaurantBudget) => {
        const dailyTaxSum = taxService.dailyTaxSum(tax, endRestaurantBudget, startRestaurantBudget);
        const message = `
    DAILY TAX: ${dailyTaxSum}             
    Daily tips tax: ${taxService.getDailyCollectedTipsTax()} 
    Daily most popular basic ingredient: ${statisticService.getDailyMostPopularIngredient()}
    Daily most profitable dish: ${statisticService.getDailyMostProfitDish().ingredient} - total: ${statisticService.getDailyMostProfitDish().total}
    Daily dishes total: ${statisticService.getDailyDishesTotal()}
    Daily most recommended dish: ${statisticService.getDailyMostPopularRecommendation()}
           
                AUDIT END`
        fileReader.appendFile(filePathForAudit, message)
    };

    writeAudit = (tax) => {
        this.init();
        this.auditData.splice(0, 1);

        const sortedData = this.sortDataByTime();

        sortedData.forEach(audit => {
            const budgetRes = audit.budget > 0 ? audit.budget : 'RESTAURANT BANKRUPT';

            const message =
                `command: => ${audit.res}
            Warehouse: ${JSON.stringify(audit.warehouses)}
            Restaurant Budget: ${budgetRes}
            All Transaction Tax: ${audit.transactionTax}
            Trash: ${JSON.stringify(audit.trash)}
            Tips: ${JSON.stringify(audit.tips)}
            Time: ${audit.time}`;
            fileReader.appendFile(filePathForAudit, message)
        })

        const endRestaurantBudget = this.auditData[this.auditData.length - 1].budget;
        this.end(tax, endRestaurantBudget, 500);
    }

    addToAudit = (audit) => {
        this.auditData.push(audit);
    }
}

const audit = new Audit();

module.exports = audit;
