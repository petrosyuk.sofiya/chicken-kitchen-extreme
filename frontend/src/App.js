import logo from './logo.svg';
import './App.css';
import {useEffect} from "react";
import {
    Chart,
    ArcElement,
    LineElement,
    BarElement,
    PointElement,
    BarController,
    BubbleController,
    DoughnutController,
    LineController,
    PieController,
    PolarAreaController,
    RadarController,
    ScatterController,
    CategoryScale,
    LinearScale,
    LogarithmicScale,
    RadialLinearScale,
    TimeScale,
    TimeSeriesScale,
    Decimation,
    Filler,
    Legend,
    Title,
    Tooltip,
    SubTitle
} from 'chart.js';

Chart.register(
    ArcElement,
    LineElement,
    BarElement,
    PointElement,
    BarController,
    BubbleController,
    DoughnutController,
    LineController,
    PieController,
    PolarAreaController,
    RadarController,
    ScatterController,
    CategoryScale,
    LinearScale,
    LogarithmicScale,
    RadialLinearScale,
    TimeScale,
    TimeSeriesScale,
    Decimation,
    Filler,
    Legend,
    Title,
    Tooltip,
    SubTitle
);

function App() {

    useEffect(async () => {

        try {
            const response = await fetch('./statistic/baseIngredientStatistic.json', {
                method: 'GET',
            });
            const result = await response.json();
            const labels = Object.keys(result);

            const dataSets = [];
            for (const ingredients of Object.values(result)) {
                for (const ingredient of ingredients) {
                    let exist = false;
                    for(const dataSet of dataSets) {
                        if (dataSet.label === ingredient.ingredient) {
                            exist = true;
                            dataSet.data.push(ingredient.qty);
                        }
                    }
                    if (!exist) {
                        dataSets.push({
                            data: [ingredient.qty],
                            label: ingredient.ingredient,
                            borderColor: '#' + Math.floor(Math.random()*16777215).toString(16),
                            fill: false
                        })
                    }
                }
            }
            const bis = document.getElementById('bis');
            const myChart = new Chart(bis, {
                type: 'line',
                data: {
                    labels: labels,
                    datasets: dataSets
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        } catch (error) {
            console.error(error);
        }
    });

    return (
        <div className="App">
            <canvas id="bis" width="200" height="200"></canvas>
        </div>
    );
}

export default App;
