const helpers = require("../helpers/helpers");
const kitchenHandler = require("../handlers/kitchenHandler");
const jsonData = require("../resources/input_files/data.json");
const command = require("../resources/input_files/commandConfiguration.json");
const orderAction = require('../actions/orderAction');
const budgetAction = require('../actions/budgetAction');
const tableAction = require('../actions/tableAction');
const buyAction = require('../actions/buyAction');
const auditAction = require('../actions/auditAction');
const throwTrashAwayAction = require('../actions/throwTrashAwayAction');
const trashService = require('../servises/trashService');
const warehouseHandler = require('../servises/warehousesHandler');
const statisticService = require('../servises/statisticService');
const timeService = require("../servises/timeService");
const orderService = require("../servises/orderService");

const filePathForOutput = './resources/output_files/output.txt';
const customers = Object.keys(jsonData['Regular customer']);
const dishes = Object.keys(jsonData.Food);
const baseIngredient = jsonData["Base ingredients"];
const trash = trashService.getTrash();

//

const calculateTime = (newArr) => {
    let time;
    const timeToCheckSpoiling = timeService.checkEvery100units();
    const arr = [];
    for (const i of newArr) {
        let action = i[0];
        switch (action) {
            case 'Buy' :
                // Time
                time = timeService.getTime(action, i);
                timeService.setTimeToBuildFood();
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
                break;
            case 'Order' :
                // need fix
                const inputArrays = orderService.dividedArray(i);
                const amount = inputArrays.length;
                const coefficient = 1.05 ** amount;
                time = timeService.getTime(action, null, amount, coefficient);
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
                break;
            case 'Budget' :
                time = timeService.getTime(action);
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
                break;
            case 'Table' :
                //need fix
                time = timeService.getTime(action, i, null, null, 2); //need fix
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
                break;
            case 'Audit' :
                time = timeService.getTime(action);
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
                break;
            case 'Throw trash away' :
                time = timeService.getTime(action);
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
                break;
            default:
                time = timeService.getTime(action);
                timeService.addTimeToTotal(time);
                timeToCheckSpoiling > 0 ? arr.push({i: `Spoiling ${timeToCheckSpoiling}`, time: time+1}) : console.log('!==100');
                arr.push({i, time})
        }
    }
    return arr;
}

const sortDataByTime = (newArr) => {
    Array.prototype.sortBy = function(p) {
        return this.slice(0).sort(function(a,b) {
            return (a[p] > b[p]) ? 1 : (a[p] < b[p]) ? -1 : 0;
        });
    }

    const newArrWithTime = calculateTime(newArr);
    const sortedNewArrWithTime = newArrWithTime.sortBy('time');
    let newArray = [];
    sortedNewArrWithTime.forEach(e => newArray.push(e.i));
    return newArray;
}
//

const main = (newArr) => {
    const sortedArray = sortDataByTime(newArr);
    for (const i of sortedArray) {
        if (i.length >= 3 || i[0] === 'Audit' || i[0] === 'Throw trash away' || i[0] === 'Spoiling' ) {
            const validBudget = kitchenHandler.checkRestaurantBudget();

            let action = i[0];
            switch (action) {
                case 'Buy' :
                    buyAction.buyAction(i, validBudget, filePathForOutput, dishes, trash)
                    break;
                case 'Order' :
                    orderAction.orderAction(i, validBudget, filePathForOutput, command, dishes, baseIngredient, trash)
                    break;
                case 'Budget' :
                    budgetAction.budgetAction(i, trash, filePathForOutput)
                    break;
                case 'Table' :
                    tableAction.tableAction(i, validBudget, customers, dishes, filePathForOutput, trash)
                    break;
                case 'Audit' :
                    auditAction.auditAction(i)
                    break;
                case 'Throw trash away' :
                    throwTrashAwayAction.throwTrashAwayAction(i, filePathForOutput)
                    break;
                case 'Spoiling' :
                    console.log('Spoiling');
                    break;
                default:
                    helpers.disabler(i)
            }
            statisticService.addToBaseIngredientStatistic(warehouseHandler.getWarehouses(), action);
        }
    }
};

module.exports = { main }