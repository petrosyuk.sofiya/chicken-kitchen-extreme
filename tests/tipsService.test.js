const tipsService = require("../servises/tipsService");
const RandomService = require("../servises/randomService");
const {expect} = require("chai");
describe('class TipsService', () => {
	test('check checkTips function, ' +
		'need to return correct tips amount(30) ' +
		'due to params(tips will be(based on mocked Randomizer), total sum 300)', () => {
		RandomService.getRandom =
			jest.fn(RandomService.getRandom).mockImplementationOnce(function() {
			return 0.6;
		})
		expect(tipsService.checkTips(500, 300, RandomService, 10)).equal(30);
	});

	test('check checkTips function, ' +
		'need to return 0(will no be tips (based on mocked randomizer))', () => {
		RandomService.getRandom =
			jest.fn(RandomService.getRandom).mockImplementationOnce(function() {
				return 0;
			})
		expect(tipsService.checkTips(500, 300, RandomService, 10)).equal(0);
	});
});