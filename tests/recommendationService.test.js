const recommendationService = require("../servises/recommendationService");
const buyService = require("../servises/buyService");

describe('class RecommendationService', () => {
	beforeEach(() => {

	});
	test('check getRecommendDish function, ' +
		'need to return Tuna Cake(most expensive dish) (when we now recommendation for client(Chocolate))', () => {

		expect(recommendationService.getRecommendDish(
			{
				"Fat Cat Chicken": ["Princess Chicken", "Youth Sauce", "Fries", "Diamond Salad"],
				"Princess Chicken": ["Chicken", "Youth Sauce"],
				"Youth Sauce": ["Asparagus", "Milk", "Honey"],
				"Spicy Sauce": ["Paprika", "Garlic", "Water"],
				"Omega Sauce": ["Lemon", "Water"],
				"Diamond Salad": ["Tomatoes", "Pickles", "Feta"],
				"Ruby Salad": ["Tomatoes", "Vinegar", "Chocolate"],
				"Fries": ["Potatoes"],
				"Smashed Potatoes": ["Potatoes"],
				"Tuna Cake": ["Tuna", "Chocolate", "Youth Sauce"],
				"Fish In Water": ["Tuna", "Omega Sauce", "Ruby Salad"],
				"Irish Fish": ["Tuna", "Fries", "Smashed Potatoes"]
			},
			[
				"Chicken", "Tuna", "Potatoes",
				"Asparagus", "Milk", "Honey",
				"Paprika", "Garlic", "Water",
				"Lemon", "Tomatoes", "Pickles",
				"Feta", "Vinegar", "Rice",
				"Chocolate"],
			'Alexandra Smith',
			['Chocolate'],
			40,
			buyService).order).toEqual("Tuna Cake");
	});

});