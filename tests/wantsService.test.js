const wantsService = require("../servises/wantsService");
const RandomService = require("../servises/randomService");

describe('class WantsService', () => {
	test('check getWants function, ' +
		'need to return equal wants(Milk, Honey) and tips (36) (when client buy Princess Chicken total 90 ant start tips 0)', () => {
		RandomService.getRandomWants =
			jest.fn(RandomService.getRandomWants).mockImplementationOnce(function() {
				return 0.86;
			})

		wantsService.getConcreteIngredientsForCheck =
			jest.fn(wantsService.getConcreteIngredientsForCheck).mockImplementationOnce(function() {
				return ['Milk', 'Honey', 'Paprika'];
			})

		expect(wantsService.getWants(
			500,
			RandomService,
			[
				"Chicken", "Tuna", "Potatoes",
				"Asparagus", "Milk", "Honey",
				"Paprika", "Garlic", "Water",
				"Lemon", "Tomatoes", "Pickles",
				"Feta", "Vinegar", "Rice",
				"Chocolate"],
			["Chicken", "Asparagus", "Milk", "Honey"], 0, 90)).toStrictEqual({"ingredients": ["Milk", "Honey"], "tipsAmount": 36});
	});

	test('check getWants function, ' +
		'need to return false (when client buy Princess Chicken total 90 ant start tips 0 => 50% 0 wants from random)', () => {
		RandomService.getRandomWants =
			jest.fn(RandomService.getRandomWants).mockImplementationOnce(function() {
				return 0.1;
			})

		wantsService.getConcreteIngredientsForCheck =
			jest.fn(wantsService.getConcreteIngredientsForCheck).mockImplementationOnce(function() {
				return ['Milk', 'Honey', 'Paprika'];
			})

		expect(wantsService.getWants(
			500,
			RandomService,
			[
				"Chicken", "Tuna", "Potatoes",
				"Asparagus", "Milk", "Honey",
				"Paprika", "Garlic", "Water",
				"Lemon", "Tomatoes", "Pickles",
				"Feta", "Vinegar", "Rice",
				"Chocolate"],
			["Chicken", "Asparagus", "Milk", "Honey"], 0, 90)).toStrictEqual(false);
	});
});